<?php
/**
 * Plugin Name: ShMapper Driver
 * Plugin URI: http://genagl.ru/?p=652
 * Description: added functional for sinchronization ShMapper and Google Drive
 * Version: 1.3.5
 * Author: genag1@list
 * Author URI:  https://genagl.ru
 * License: GPL2
 * Text Domain: shmapper-drive
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Contributors:
	Genagl (genag1@list.ru)

 * License: GPLv2 or later
	Copyright 2020  Genagl  (email: genag1@list.ru)

	GNU General Public License, Free Software Foundation <http://www.gnu.org/licenses/gpl-2.0.html>

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

// load textdomain
function init_textdomain_shmapperd() 
{ 
	if (function_exists('load_plugin_textdomain')) 
	{
		load_plugin_textdomain("shmapper-drive", false , dirname( plugin_basename( __FILE__ ) ) .'/lang/');     
	} 
}
add_action('plugins_loaded', 'init_textdomain_shmapperd');

//Paths
define('SHMD_URLPATH',  '/wp-content/plugins/' . plugin_basename(dirname(__FILE__)) . '/');
define('SHMD_REAL_PATH', WP_PLUGIN_DIR.'/'.plugin_basename(dirname(__FILE__)).'/');
define('SHMAPPERD', 'shmapper-drive');
define('SHMAPPER_POINT_MESSAGE', 'shm_point_msg');

require_once(SHMD_REAL_PATH.'class/ShMapperDrive.class.php');

register_activation_hook( __FILE__, array( 'ShMapperDrive', 'activate' ) );
if (function_exists('register_deactivation_hook'))
{
	register_deactivation_hook(__FILE__, array('ShMapperDrive', 'deactivate'));
}
add_action("init", "init_shmapperDrive", 2);
function init_shmapperDrive()
{
	require_once(SHMD_REAL_PATH.'class/ShMapperDrive_ajax.class.php');
	require_once(SHMD_REAL_PATH.'class/ShMapperPointMessage.class.php');
	ShMapperDrive::get_instance();
	ShMapperDrive_ajax::get_instance();
	ShMapperPointMessage::init();
} 
